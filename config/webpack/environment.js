// const { environment } = require('@rails/webpacker')

// module.exports = environment

const { environment } = require('@rails/webpacker');
var webpack = require('webpack');
environment.plugins.append(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
  })
)

const erb = require('./loaders/erb');
environment.loaders.append('erb', erb);

module.exports = environment

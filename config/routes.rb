Rails.application.routes.draw do
  get 'news/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
  	resources :news, only: [:index]
  	get 'news/show', to: 'news#show'
  end

  resources :news, only: [:index]
  get 'news/show', to: 'news#show'
  get 'news/get_topic', to: 'news#get_topic'

  root to: 'news#index'
end

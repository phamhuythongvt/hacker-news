class ApplicationService
  def self.call(*args, &block)
    new(*args, &block).call
  end

  private

  def initialize(*args)
    return if args.empty?
  end
end

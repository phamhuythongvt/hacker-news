# frozen_string_literal: true

class News::GetTopicService < ApplicationService
  require 'readability'
  require 'open-uri'

  def call
    source = URI.open(@url).read
    rbody = Readability::Document.new(source, tags: %w[h1 h2 div p img a], source: %w[src href])
    data = {}
    data[:title] = rbody.title
    data[:content] = rbody.content
    data[:image] = rbody.images.first || ''
    data
  end

  private

  def initialize(url)
    super(url)
    @url = url
  end
end

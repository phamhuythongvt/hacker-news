# frozen_string_literal: true

class News::FetchNewsService < ApplicationService
  require 'open-uri'
  require 'nokogiri'

  def call
    data = {}
    doc = Nokogiri::HTML(URI.open(@url))
    topics = doc.search('.itemlist tr.athing')
    votes = doc.search('.itemlist tr td.subtext')
    data[:items] = parse_doc_to_items(topics, votes)
    data[:more] = doc.search('.itemlist tr td a.morelink')
    data
  end

  private

  def initialize(url)
    super(url)
    @url = url
  end

  def parse_doc_to_items(topics, votes)
    items = []
    topics.each_with_index do |topic, index|
      item = {}
      topic_link(topic, item)
      link_note(topic, item)
      item[:votes] = votes[index].inner_text
      items.push << item
    end
    items
  end

  def topic_link(topic, item)
    topic_link = topic.at('td.title a.storylink')
    item[:link] = topic_link['href']
    item[:title] = topic_link.inner_text
  end

  def link_note(topic, item)
    item[:link_note] = topic.search('td.title span.sitebit').inner_text
  end
end

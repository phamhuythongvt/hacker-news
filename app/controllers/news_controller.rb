class NewsController < ApplicationController
  def index
    return unless request.xhr?

    @data = News::FetchNewsService.call(params[:url])
  end

  def show
    return unless request.xhr?

    @data = News::GetTopicService.call(params[:topic])
  end

  def get_topic
    @data = News::GetTopicService.call(params[:url])
    render json: @data
  end
end

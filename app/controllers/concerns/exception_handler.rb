module ExceptionHandler
  extend ActiveSupport::Concern

  private

  def render_error(error, status)
    json_response({ message: error[:message], description: error[:description] }, status)
  end
end

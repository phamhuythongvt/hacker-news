class Api::V1::BaseController < ActionController::API
  include JsonResponse
  include ExceptionHandler
end

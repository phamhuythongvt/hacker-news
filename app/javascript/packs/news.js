var current_news_page = 1;
var news_url = 'https://news.ycombinator.com/best?p='

$(function(e) {
  loadNews();
})

$('.more-link').on('click', (e) => {
  $('.loading-col').show();
  $('.more-col').addClass('hide')
  loadNews();
})

function loadNews(){
  $.ajax({
    url: this.href,
    dataType: 'script',
    data: {url: news_url + current_news_page},
    success: function(response){
      current_news_page += 1
    }
  });
}
